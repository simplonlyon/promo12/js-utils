
## How To use
1. Cloner le dépôt
2. Aller dans le dossier avec le terminal
3. Faire un `npm install`
4. Lancer le `npm run dev` pour lancer webpack
5. (Optionnel) Dans un autre terminal, lancer le `npm run serve` pour lancer le serveur de développement
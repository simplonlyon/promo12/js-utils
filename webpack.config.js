module.exports = {
    //On peut définir le point d'entrée de notre application
    // entry: './src/app.js',
    //On peut définir le fichier généré par webpack
    // output: 'bundle.js'
    //On définit le mode development qui rendra le code plus facilement débuggable
    //et qui a un temps de compilation pour rapide
    mode: 'development',
    //On indique les sourceMaps qui permettent de faire la correspondance
    //entre les lignes du code source et celles du fichier compilé
    devtool: 'inline-source-map',
    module: {
        //Ici on définit une règle pour les imports de webpack
        rules: [
          {
            //Si on import un fichier se terminant par .sass ou .scss
            test: /\.s[ac]ss$/i,
            //On lui dit d'utiliser les loaders suivant
            use: [
              // Creates `style` nodes from JS strings
              'style-loader',
              // Translates CSS into CommonJS
              'css-loader',
              // Compiles Sass to CSS
              'sass-loader',
            ],
          },
        ],
      },
}